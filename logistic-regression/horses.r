source("logReg.r")


# Training data.
train.data.frame <- read.table("horseColicTraining.txt")
train.data.mat <- data.matrix(select(train.data.frame, 1:21))
train.data.classes <- train.data.frame[[22]]


# Test data.
test.data.frame <- read.table("horseColicTest.txt")
test.data.mat <- data.matrix(select(test.data.frame, 1:21))
test.data.classes <- test.data.frame[[22]]

test.rows <- dim(test.data.frame)[1]
test.cols <- dim(test.data.frame)[2]


# Get weights and obtains error ratio.
colic.test <- function() {
  weights <- sga(train.data.mat, train.data.classes, 1000)
  error.count <- 0
  
  for (i in 1:test.rows) {
    curr.obs <- test.data.mat[i,]
    expected.class <- test.data.classes[i]
    if (classify.vector(curr.obs, weights) != expected.class) {
      error.count <- error.count + 1
    }
  }
  
  error.rate = error.count/test.rows
  printf("Error rate: %f\n", error.rate)
  error.rate
}


# Calculates average error ratio.
multiple.test <- function() {
  quantity <- 10
  error.sum <- 0
  for (i in 1:quantity) {
    error.sum <- error.sum + colic.test()
  }
  printf("After %d iterations the average error rate is %f", quantity, error.sum/quantity)
}


multiple.test()


