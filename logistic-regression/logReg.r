library(dplyr)

printf <- function(...) cat(sprintf(...))

sigmoid <- function(in.x) {
  1.0/(1+exp(-in.x))
}

classify.vector <- function(in.x, weights) {
  prob <- sigmoid(sum(in.x*weights))
  ifelse(prob > 0.5, 1.0, 0.0)
}

sga <- function(data, class.labels, num.iter=150) {
  m <- dim(data)[1]
  n <- dim(data)[2]
  
  weights <- rep(1, n)

  for (j in 1:num.iter ) {
    printf("Iteration %d\n", j)
    random.indices <- sample(1:m, m)
    for (i in 1:m) {
      alpha <- 4/(1.0+j+i)+0.0001
      rand.index <- random.indices[i]

      h <- sigmoid(sum(data[rand.index,]*weights))
      error <- class.labels[rand.index] - h
      weights <- weights + alpha * error * data[rand.index,]
    }
  }
  weights
}
